# -*- coding:utf-8 -*-
from .list_page import ListPage
from .scrolling_page import ScrollingPage
from .paths import Xpaths, CssPaths, Paths
from .targets import Targets
from DataRecorder import Recorder

__version__ = '1.0.5'
